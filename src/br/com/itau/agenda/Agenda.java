package br.com.itau.agenda;

import java.util.ArrayList;

public class Agenda {
    private int quantidadeDeContatos;
    private ArrayList<Pessoa> pessoas;

    public Agenda(int quantidadeDeContatos){
        this.quantidadeDeContatos = quantidadeDeContatos;
    }

    public Agenda(int quantidadeDeContatos, ArrayList<Pessoa> pessoas) {
        this.quantidadeDeContatos = quantidadeDeContatos;
        pessoas = new ArrayList();
    }

    public int getQuantidadeDeContatos() {
        return quantidadeDeContatos;
    }

    public void incluirContato(ArrayList<Pessoa> pessoas, int quantidade){
        for(int i = 0; i < quantidade; i++) {
            Pessoa pessoa = new Pessoa();

            System.out.println("Digite o nome da pessoa " + (i + 1) + ": ");
            pessoa.setNome(Leitor.lerNome());

            System.out.println("Digite o e-mail da pessoa " + (i + 1) + ": ");
            pessoa.seteMail(Leitor.lerEmail());

            pessoas.add(pessoa);
        }
    }

    public void incluirContatoNovo(ArrayList<Pessoa> pessoas){
        Pessoa pessoa = new Pessoa();

        System.out.println("Digite o nome da pessoa: " + (pessoas.size() + 1) + ": ");
        pessoa.setNome(Leitor.lerNome());

        System.out.println("Digite o e-mail da pessoa " + (pessoas.size() + 1) + ": ");
        pessoa.seteMail(Leitor.lerEmail());

        pessoas.add(pessoa);
    }

    public Pessoa pesquisarContatoNome(String nome, ArrayList<Pessoa> pessoas){
        Pessoa pessoa = new Pessoa();

        for(int i = 0; i < pessoas.size(); i++){
            if(nome.equals(pessoas.get(i).getNome())){
                pessoa = pessoas.get(i);
                return pessoa;
            }
        }

        return null;
    }

    public Pessoa pesquisarContatoEmail(String eMail, ArrayList<Pessoa> pessoas){
        Pessoa pessoa = new Pessoa();

        for(int i = 0; i < pessoas.size(); i++){
            if(eMail.equals(pessoas.get(i).getEmail())){
                pessoa = pessoas.get(i);
                return pessoa;
            }
        }

        return null;
    }

    public void excluirContatoNome(String nome, ArrayList<Pessoa> pessoas){
        boolean excluido = false;

        for(int i = 0; i < pessoas.size(); i++){
            if(nome.equals(pessoas.get(i).getNome())){
                pessoas.remove(i);
                Impressora.imprimirMensagemDeExclusao();
                excluido = true;
            }
        }

        if(!excluido){
            Impressora.imprimirMensagemNaoEncontrado();
        }
    }

    public void excluirContatoEmail(String eMail, ArrayList<Pessoa> pessoas){
        boolean excluido = false;

        for(int i = 0; i < pessoas.size(); i++){
            if(eMail.equals(pessoas.get(i).getEmail())){
                pessoas.remove(i);
                Impressora.imprimirMensagemDeExclusao();
                excluido = true;
            }
        }

        if(!excluido){
            Impressora.imprimirMensagemNaoEncontrado();
        }
    }
}
