package br.com.itau.agenda;

import java.util.Scanner;

public class Leitor {
    private static Scanner scanNome, scanEmail, scanQuantidade, scanOpcao;

    public static int lerQuantidade(){
        scanQuantidade = new Scanner(System.in);
        System.out.println("Digite a quantidade de contatos a serem inseridos: ");
        int quantidade = scanQuantidade.nextInt();
        return quantidade;
    }

    public static String lerNome(){
        scanNome = new Scanner(System.in);
        String nome = scanNome.next();
        return nome;
    }
    public static String lerEmail(){
        scanEmail = new Scanner(System.in);
        String eMail = scanEmail.next();
        return eMail;
    }

    public static int lerOpcao(){
        System.out.print("Selecionar: ");
        scanOpcao = new Scanner(System.in);
        int opcao = scanOpcao.nextInt();
        return opcao;
    }
}
