package br.com.itau.agenda;

import java.util.ArrayList;

public class AppAgenda {
    public static void main(String[] args) {
        int quantidade = Leitor.lerQuantidade();

        Agenda agenda = new Agenda(quantidade);
        ArrayList<Pessoa> pessoas = new ArrayList();

        agenda.incluirContato(pessoas, quantidade);

        OrquestradorAgenda.direcionarRequisicao(agenda, pessoas);
    }
}
