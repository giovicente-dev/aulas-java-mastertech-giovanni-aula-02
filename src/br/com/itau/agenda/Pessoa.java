package br.com.itau.agenda;

public class Pessoa {
    private String nome, eMail;

    public Pessoa(String nome, String eMail){
        this.nome = nome;
        this.eMail = eMail;
    }

    public Pessoa(){}

    public String getNome() {
        return nome;
    }

    public String getEmail() {
        return eMail;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }
}
