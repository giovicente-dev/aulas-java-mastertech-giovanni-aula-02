package br.com.itau.agenda;

import java.util.ArrayList;

public class Impressora {
    public static void imprimirMenuPrincipal(){
        System.out.println("*******************************************");
        System.out.println("Após a carga, selecione a opção desejada");
        System.out.println("1 - Pesquisar por nome");
        System.out.println("2 - Pesquisar por e-mail");
        System.out.println("3 - Excluir por nome");
        System.out.println("4 - Excluir por e-mail");
        System.out.println("5 - Retornar todos os contatos");
        System.out.println("6 - Incluir novo contato");
        System.out.println("9 - Sair");
        System.out.println("*******************************************");
    }

    public static void imprimirConsulta(Pessoa pessoa){
        if(pessoa != null) {
            System.out.println(pessoa.getNome() + ", " + pessoa.getEmail());
        } else {
            Impressora.imprimirMensagemNaoEncontrado();
        }
    }

    public static void imprimirAgenda(ArrayList<Pessoa> pessoas){
        for(int i = 0; i < pessoas.size(); i++){
            System.out.println(pessoas.get(i).getNome() + ", " + pessoas.get(i).getEmail());
        }
    }

    public static void imprimirMensagemDeExclusao(){
        System.out.println("Registro excluído com sucesso!");
    }

    public static void imprimirMensagemNaoEncontrado(){
        System.out.println("Registro não encontrado.");
    }
}
