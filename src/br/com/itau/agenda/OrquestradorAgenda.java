package br.com.itau.agenda;

import java.util.ArrayList;

public class OrquestradorAgenda {

    public static void direcionarRequisicao(Agenda agenda, ArrayList<Pessoa> pessoas){
        int opcao = 0;

        do {
            Impressora.imprimirMenuPrincipal();
            opcao = Leitor.lerOpcao();

            switch (opcao) {
                case 1:
                    System.out.print("Digite o nome: ");
                    String nome = Leitor.lerNome();
                    Pessoa pessoaNome = new Pessoa();
                    pessoaNome = agenda.pesquisarContatoNome(nome, pessoas);
                    Impressora.imprimirConsulta(pessoaNome);
                    break;

                case 2:
                    System.out.print("Digite o e-mail: ");
                    String eMail = Leitor.lerEmail();
                    Pessoa pessoaEmail = new Pessoa();
                    pessoaEmail = agenda.pesquisarContatoEmail(eMail, pessoas);
                    Impressora.imprimirConsulta(pessoaEmail);
                    break;

                case 3:
                    System.out.print("Digite o nome: ");
                    nome = Leitor.lerNome();
                    agenda.excluirContatoNome(nome, pessoas);
                    break;

                case 4:
                    System.out.println("Digite o e-mail: ");
                    eMail = Leitor.lerEmail();
                    agenda.excluirContatoEmail(eMail, pessoas);
                    break;

                case 5:
                    Impressora.imprimirAgenda(pessoas);
                    break;

                case 6:
                    agenda.incluirContatoNovo(pessoas);
                    break;
            }
        } while (opcao != 9);
    }
}
